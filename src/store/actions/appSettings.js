import { MAX_MOBILE_SCREEN, MAX_TABLET_SCREEN } from 'utils/const/appSettings';
import {
  APP_SETTINGS_RESIZE_SCREEN,
  APP_SETTINGS_SWITCH_ASIDE_MENU,
  APP_SETTINGS_SET_LANG,
  APP_SETTINGS_SET_VOLUME_ON,
  APP_SET_LOADING
} from '../actionTypes.js';

export const setScreen = (
  isMobile,
  isNotMobile,
  isTablet,
  isDesktop,
  showAsideMenu
) => ({
  type: APP_SETTINGS_RESIZE_SCREEN,
  isMobile,
  isNotMobile,
  isTablet,
  isDesktop,
  showAsideMenu
});

export const setLoading = isLoadingApp => ({
  type: APP_SET_LOADING,
  isLoadingApp
});

export const setLang = lang => ({
  type: APP_SETTINGS_SET_LANG,
  lang
});

export const setShowAsideMenu = showAsideMenu => ({
  type: APP_SETTINGS_SWITCH_ASIDE_MENU,
  showAsideMenu
});

export const setVolumeOn = volumeOn => ({
  type: APP_SETTINGS_SET_VOLUME_ON,
  volumeOn
});

export const resizeScreen = () => dispatch => {
  const widthScreen =
    window.innerWidth || //  узнать текущий размер окна
    document.documentElement.clientWidth ||
    document.body.clientWidth;

  const isMobile = widthScreen <= MAX_MOBILE_SCREEN;
  const isNotMobile = !isMobile;
  const isTablet = widthScreen <= MAX_TABLET_SCREEN && !isMobile;
  const isDesktop = !(isMobile || isTablet);
  const showAsideMenu = isDesktop;

  dispatch(
    setScreen(isMobile, isNotMobile, isTablet, isDesktop, showAsideMenu)
  );
};

export const checkLang = () => dispatch => {
  let lang = window.navigator
    ? window.navigator.language ||
      window.navigator.systemLanguage ||
      window.navigator.userLanguage
    : `ru`;
  lang = lang.substr(0, 2).toLowerCase();

  // TODO хардкор русского языка, допилить определение языка на клиенте
  lang = `ru`;

  dispatch(setLang(lang));
};

export const switchMenu = show => dispatch => {
  const showAsideMenu = show;

  dispatch(setShowAsideMenu(showAsideMenu));
};
