import {
  APP_SETTINGS_RESIZE_SCREEN,
  APP_SETTINGS_SWITCH_ASIDE_MENU,
  APP_SETTINGS_SET_LANG,
  APP_SETTINGS_SET_VOLUME_ON,
  APP_SET_LOADING
} from '../actionTypes';

const initialState = {
  isLoadingApp: true, // приложение загружается при старте (проверка токена в localStorage)

  // версия отображения сайта mobile, tabled, desktop
  isMobile: false, //
  isNotMobile: true, //
  isTablet: false, //
  isDesktop: true, //

  showAsideMenu: false, // показ бокового меню
  lang: `en`, // язык преложения = `ru`  `en'

  volumeOn: true
};

export default (state = initialState, action) => {
  switch (action.type) {
    case APP_SETTINGS_RESIZE_SCREEN:
      return {
        ...state,
        isMobile: action.isMobile,
        isNotMobile: action.isNotMobile,
        isTablet: action.isTablet,
        isDesktop: action.isDesktop,
        showAsideMenu: action.showAsideMenu
      };
    case APP_SETTINGS_SET_LANG:
      return {
        ...state,
        lang: action.lang
      };
    case APP_SET_LOADING:
      return {
        ...state,
        isLoadingApp: action.isLoadingApp
      };
    case APP_SETTINGS_SWITCH_ASIDE_MENU:
      return {
        ...state,
        showAsideMenu: action.showAsideMenu
      };
    case APP_SETTINGS_SET_VOLUME_ON:
      return {
        ...state,
        volumeOn: action.volumeOn
      };
    default:
      return state;
  }
};
