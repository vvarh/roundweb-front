const initialState = {
  links: [] // { to, text, img }
};

export default (state = initialState, action) => {
  switch (action.type) {
    default:
      return state;
  }
};
