import { combineReducers } from 'redux';
import appSettings from './reducers/appSettings.js';
import menuLinks from './reducers/menuLinks.js';

export default combineReducers({
  appSettings,
  menuLinks
});
