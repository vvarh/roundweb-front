import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Router from '../routers/Router';
import { throttle } from '../utils/func/util.js';
import { resizeScreen, checkLang } from '../store/actions/appSettings.js';

class AppMain extends React.Component {
  componentDidMount() {
    // check login after start app
    // if (this.props.autoLoginAction) this.props.autoLoginAction(); // for test or developer - process.env.NODE_ENV === 'production'
    if (this.props.checkLangAction) this.props.checkLangAction();

    // проверка размеров окна при старте приложения
    if (this.props.resizeScreenAction) this.props.resizeScreenAction();

    // слежение за размером окна при ресайзе
    const throttledResize = throttle(() => {
      this.props.resizeScreenAction();
    }, 300);

    window.onresize = () => {
      throttledResize();
    };

    // console.error(`test sentry`);
  }

  render() {
    return (
      <>
        <Router />
      </>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  checkLangAction: () => dispatch(checkLang()),
  resizeScreenAction: widthScreen => dispatch(resizeScreen(widthScreen))
});

AppMain.propTypes = {
  checkLangAction: PropTypes.func,
  resizeScreenAction: PropTypes.func
};

export default connect(null, mapDispatchToProps)(AppMain);
