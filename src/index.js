import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import * as Sentry from '@sentry/browser';
import App from './App/App';
import store from './store/store';
import * as serviceWorker from './serviceWorker';

if (process.env.NODE_ENV === `production`) {
  Sentry.init({
    dsn: 'https://81b5368774984ddf9ce0c308be6f19d9@sentry.io/1636301'
  });
}

const appWithStore = (
  <Provider store={store}>
    <App />
  </Provider>
);

ReactDOM.render(appWithStore, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
// serviceWorker.unregister();
serviceWorker.register();
