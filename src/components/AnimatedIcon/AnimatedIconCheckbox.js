import React from 'react';
import styled, { keyframes } from 'styled-components';

const opacityAnim3 = keyframes`
  10% {stroke-dashoffset: 0; opacity: 1;}
  70% {stroke-dashoffset: 0; opacity: 1;}
  80% {opacity: 0;}
  100% {stroke-dashoffset: 0; opacity: 0;}
`;

const SSVGCheckbox = styled.svg`
  & path:nth-child(2) {
    stroke: #01e2a5;
    stroke-dasharray: 200;
    stroke-dashoffset: -200;
    animation: ${opacityAnim3} 8s linear infinite;
  }
`;

const AnimatedIconCheckbox = () => {
  return (
    <SSVGCheckbox
      version="1.1"
      xmlns="http://www.w3.org/2000/svg"
      x="0px"
      y="0px"
      width="50px"
      height="50px"
      viewBox="0 0 240.487 240.487"
    >
      <path
        fill="none"
        stroke="#ffffff"
        strokeWidth="8"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeMiterlimit="10"
        d="M236.487,120.244c0,64.198-52.045,116.243-116.243,116.243C56.045,236.487,4,184.442,4,120.244C4,56.045,56.045,4,120.244,4 C184.442,4,236.487,56.045,236.487,120.244z"
      />
      <path
        fill="none"
        stroke="#ffffff"
        strokeWidth="16"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeMiterlimit="10"
        d="M184.881,78.874l-83.251,83.25l-42.579-42.578"
      />
    </SSVGCheckbox>
  );
};

export default AnimatedIconCheckbox;
