import React from 'react';
import styled, { keyframes } from 'styled-components';

const opacityAnimPath1 = keyframes`
  0% {fill: none}
  5% {fill: #666}
  35% {fill: none}
`;
const opacityAnimPath2 = keyframes`
  0% {fill: none}
  5% {fill: #008800}
  35% {fill: none}
`;
const opacityAnimPath3 = keyframes`
  0% {fill: none}
  5% {fill: #008800}
  35% {fill: none}
`;

const SSVGCheckbox = styled.svg`
  & path:nth-child(1) {
    animation: ${opacityAnimPath1} 8s linear infinite;
  }
  & path:nth-child(2) {
    animation: ${opacityAnimPath2} 8s linear infinite;
  }
  & path:nth-child(3) {
    animation: ${opacityAnimPath3} 8s linear infinite;
  }
`;

const AnimatedIconMedal = () => {
  return (
    <SSVGCheckbox
      version="1.1"
      xmlns="http://www.w3.org/2000/svg"
      x="0px"
      y="0px"
      width="40px"
      height="60px"
      viewBox="0 0 192 287"
      enable-background="new 0 0 192 287"
    >
      <path
        fill="none"
        stroke="#ffffff"
        strokeWidth="8"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeMiterlimit="10"
        d="M173.073,200.754c0,42.266-34.266,76.53-76.533,76.53c-42.267,0-76.532-34.265-76.532-76.53c0-42.269,34.266-76.534,76.532-76.534 C138.808,124.22,173.073,158.485,173.073,200.754z"
      />
      <path
        fill="none"
        stroke="#ffffff"
        strokeWidth="8"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeMiterlimit="10"
        d="M134.173,186.427l-22.35-3.246l-9.992-20.252c-0.994-2.012-3.046-3.286-5.29-3.286c-2.243,0-4.293,1.273-5.289,3.286l-9.992,20.252 l-22.351,3.246c-2.22,0.325-4.065,1.879-4.758,4.014c-0.696,2.136-0.116,4.479,1.492,6.044l16.171,15.765l-3.819,22.257 c-0.378,2.215,0.53,4.448,2.347,5.769c1.816,1.319,4.225,1.495,6.212,0.448l19.987-10.509l19.988,10.509 c0.864,0.454,1.807,0.678,2.743,0.678c1.224,0,2.44-0.381,3.468-1.126c1.815-1.318,2.725-3.554,2.347-5.767l-3.818-22.259 l16.171-15.763c1.608-1.568,2.186-3.911,1.492-6.047C138.238,188.306,136.394,186.752,134.173,186.427L134.173,186.427z M110.816,205.968"
      />

      <path
        fill="none"
        stroke="#ffffff"
        strokeWidth="8"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeMiterlimit="10"
        d="M183.277,98.317H9.804V10.403h173.473V98.317z"
      />
      <path
        fill="none"
        stroke="#ffffff"
        strokeWidth="8"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeMiterlimit="10"
        d="M9.935,99.103l23.286,56.516"
      />
      <path
        fill="none"
        stroke="#ffffff"
        strokeWidth="8"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeMiterlimit="10"
        d="M183.147,99.103l-23.287,56.516"
      />
    </SSVGCheckbox>
  );
};

export default AnimatedIconMedal;
