import React from 'react';
import styled, { keyframes } from 'styled-components';

const opacityAnim3 = keyframes`
  8% {
    stroke: #fff;
    stroke-width: 8;
    }
  10% {
    stroke: #ffff00;
    stroke-width: 16;
    }
  15% {
    stroke: #fff;
    stroke-width: 8;
    }
  20% {
    stroke: #ffff00;
    stroke-width: 16;
    }
  70% {
    stroke: #ffff00;
    stroke-width: 16;
    }
  80% {
    stroke: #fff;
    stroke-width: 8;
    }
`;

const SSVGCheckbox = styled.svg`
  & path:nth-child(1) {
    stroke: #fff;
    animation: ${opacityAnim3} 10s linear 5s infinite;
  }
`;

const AnimatedIconLamp = () => {
  return (
    <SSVGCheckbox
      version="1.1"
      xmlns="http://www.w3.org/2000/svg"
      x="0px"
      y="0px"
      width="38px"
      height="60px"
      viewBox="0 0 181.491 278.986"
    >
      <path
        fill="none"
        stroke="#ffffff"
        strokeWidth="8"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeMiterlimit="10"
        d="M76.317,154.442l-21.393-53.483l14.399-19.335l20.775,15.221l20.777-15.221l14.398,19.335l-21.394,53.483"
      />
      <path
        fill="none"
        stroke="#ffffff"
        strokeWidth="8"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeMiterlimit="10"
        d="M170.846,92.269c0-44.522-36.223-80.744-80.747-80.744c-44.525,0-80.747,36.221-80.747,80.744c0,30.888,12.396,47.315,23.333,61.81 c9.235,12.24,17.212,22.81,17.212,41.387L49.892,229.4c0,19.603,15.752,35.548,35.35,35.548h9.711 c19.599,0,35.416-15.945,35.416-35.548l-0.067-33.935c0-18.577,7.974-29.147,17.211-41.387 C158.45,139.587,170.846,123.156,170.846,92.269L170.846,92.269z"
      />
      <path
        fill="none"
        stroke="#ffffff"
        strokeWidth="8"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeMiterlimit="10"
        d="M50.607,210.057h78.984"
      />
      <path
        fill="none"
        stroke="#ffffff"
        strokeWidth="8"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeMiterlimit="10"
        d="M50.607,230.627h78.984"
      />
    </SSVGCheckbox>
  );
};

export default AnimatedIconLamp;
