import React from 'react';
import styled, { keyframes } from 'styled-components';

const opacityAnim3 = keyframes`
  18% {transform: rotate(0deg)}
  20% {transform: rotate(5deg)}
  22% {transform: rotate(-5deg)}
  25% {transform: rotate(5deg)}
  27% {transform: rotate(0deg)}
`;

const SSVGCheckbox = styled.svg`
  animation: ${opacityAnim3} 10s linear 3s infinite;
`;

const AnimatedIconWallet = () => {
  return (
    <SSVGCheckbox
      version="1.1"
      xmlns="http://www.w3.org/2000/svg"
      x="0px"
      y="0px"
      width="50px"
      height="50px"
      viewBox="0 0 242 240.984"
    >
      <path
        fill="none"
        stroke="#ffffff"
        strokeWidth="8"
        strokeLinecap="round"
        strokeMiterlimit="10"
        d="M235.547,209.749 c0,13.747-11.248,24.993-24.995,24.993H31.853c-13.747,0-24.994-11.246-24.994-24.993V31.051c0-13.747,11.247-24.995,24.994-24.995 h153.702c24.734,0,49.992,24.468,49.992,49.992V209.749z"
      />
      <path
        fill="none"
        stroke="#ffffff"
        strokeWidth="8"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeMiterlimit="10"
        d="M235.547,80.663c0-13.749-11.248-24.995-24.995-24.995H31.853l0,0c-17.258,0-24.994-11.248-24.994-24.994"
      />
      <line
        fill="none"
        stroke="#ffffff"
        strokeWidth="8"
        strokeLinecap="round"
        strokeMiterlimit="10"
        x1="233.619"
        y1="145.496"
        x2="214.455"
        y2="145.496"
      />
      <path
        fill="none"
        stroke="#ffffff"
        strokeWidth="8"
        strokeLinecap="round"
        strokeMiterlimit="10"
        d="M233.251,180.126h-22.805 c-18.493,0-33.484-14.994-33.484-33.48v-1.15c0-18.485,14.991-33.478,33.484-33.478h22.805"
      />
    </SSVGCheckbox>
  );
};

export default AnimatedIconWallet;
