import React from 'react';
import styled, { keyframes } from 'styled-components';

const opacityAnim3 = keyframes`
  0% {transform: rotate(0deg)}
  2% {transform: rotate(5deg)}
  4% {transform: rotate(-5deg)}
  7% {transform: rotate(5deg)}
  9% {transform: rotate(0deg)}
`;

const SSVGCheckbox = styled.svg`
  & path:nth-child(3) {
    stroke: #fff;
    animation: ${opacityAnim3} 4s linear infinite;
  }
`;

/* 1-2 новые стрелки */
const SSVGLine3 = styled.svg`
  & path:nth-child(1) {
    stroke: #fff;
    transform: ${props => `rotate(${props.hDeg}deg)`};
    transition: transform 1s;
  }
  & path:nth-child(2) {
    stroke: #fff;
    transform: ${props => `rotate(${props.mDeg}deg)`};
    transition: transform 1s;
  }
`;

const HOUR_DEG = 30;
const HOUR_MINUTS_DEG = 0.5; // чтобы сдвинут стрелка час на 1 час - тиаками минут
const MIN_DEG = 6;
const HOUR_DEFAULT = 300;
const MIN_DEFAULT = 60;

class AnimatedIconClock extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      hDeg: HOUR_DEFAULT,
      mDeg: MIN_DEFAULT,
      typeTime: 1,
      timeInterval: 4000
    };

    this.lastTimerID = null;
  }

  componentWillUnmount() {
    clearTimeout(this.lastTimerID);
  }

  componentDidMount() {
    this.startTimer();
  }

  startTimer() {
    clearTimeout(this.lastTimerID);
    this.lastTimerID = setTimeout(
      () => this.calcRotate(),
      this.state.timeInterval
    );
  }

  calcRotate = () => {
    this.setState(prevState => {
      let { typeTime } = prevState;
      let hDeg;
      let mDeg;

      switch (typeTime) {
        case 1: {
          const time = new Date();
          const hour = time.getHours();
          const min = time.getMinutes();

          hDeg = HOUR_DEG * hour + min * HOUR_MINUTS_DEG;
          mDeg = MIN_DEG * min;
          typeTime = 0;
          break;
        }

        // typeTime === 0
        default: {
          hDeg = HOUR_DEFAULT;
          mDeg = MIN_DEFAULT;
          typeTime = 1;
          break;
        }
      }

      return { hDeg, mDeg, typeTime };
    });

    this.startTimer();
  };

  render() {
    return (
      <>
        <SSVGCheckbox
          version="1.1"
          xmlns="http://www.w3.org/2000/svg"
          x="0px"
          y="0px"
          width="50px"
          height="58px"
          viewBox="0 0 206.667 243.334"
        >
          <path
            fill="none"
            stroke="#ffffff"
            strokeWidth="8"
            strokeLinecap="round"
            strokeMiterlimit="10"
            d="M200.623,138.222 c0,53.373-43.268,96.638-96.641,96.638c-53.375,0-96.642-43.266-96.642-96.638c0-53.375,43.267-96.643,96.642-96.643 C157.355,41.579,200.623,84.847,200.623,138.222z"
          />

          <path
            fill="none"
            stroke="#ffffff"
            strokeWidth="8"
            strokeLinecap="round"
            strokeMiterlimit="10"
            d="M43.998,215.118 l-21.771,21.77 M185.737,236.887l-21.771-21.77"
          />

          <path
            fill="none"
            stroke="#ffffff"
            strokeWidth="8"
            strokeLinecap="round"
            strokeMiterlimit="10"
            d="M67.688,48.364 c-1.242-13.335,4.938-26.855,17.092-34.389c17.109-10.604,39.574-5.332,50.179,11.775c3.928,6.337,5.677,13.412,5.454,20.346"
          />

          <SSVGLine3
            version="1.1"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="-25 -30 50 50"
            hDeg={this.state.hDeg}
            mDeg={this.state.mDeg}
          >
            <path
              fill="none"
              stroke="#ffffff"
              strokeWidth="4"
              d="M0,0 L0,-15"
            />
            <path
              fill="none"
              stroke="#ffffff"
              strokeWidth="2"
              d="M0,0 L0,-20"
            />
          </SSVGLine3>
        </SSVGCheckbox>
      </>
    );
  }
}

export default AnimatedIconClock;
