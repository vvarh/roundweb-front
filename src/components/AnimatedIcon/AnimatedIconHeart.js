import React from 'react';
import styled, { keyframes } from 'styled-components';

const opacityAnim3 = keyframes`
  18% {transform: scale(1)}
  20% {transform: scale(1.1)}
  22% {transform: scale(0.8)}
  25% {transform: scale(1.1)}
  27% {transform: scale(1)}
`;

const SSVGCheckbox = styled.svg`
  animation: ${opacityAnim3} 6s linear infinite;
`;

const AnimatedIconHeart = () => {
  return (
    <SSVGCheckbox
      version="1.1"
      xmlns="http://www.w3.org/2000/svg"
      x="0px"
      y="0px"
      width="55px"
      height="50px"
      viewBox="0 0 265.333 239"
      enable-background="new 0 0 265.333 239"
    >
      <path
        fill="none"
        stroke="#ffffff"
        strokeWidth="8"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeMiterlimit="10"
        d="M22.98,116.188l109.846,118.391l109.855-118.391c22.599-25.075,22.583-65.827-0.051-90.887l-1.774-1.965 C230.182,11.52,216.081,5.02,201.144,5.017c-14.94,0-29.043,6.502-39.708,18.319L140.19,46.859 c-1.881,2.077-4.557,3.268-7.359,3.265c-2.805,0.003-5.483-1.188-7.363-3.265l-21.246-23.528 C93.553,11.522,79.453,5.017,64.518,5.02c-14.939-0.003-29.043,6.505-39.716,18.314L23.027,25.3 C0.396,50.354,0.379,91.112,22.98,116.188L22.98,116.188z M22.98,116.188"
      />
    </SSVGCheckbox>
  );
};

export default AnimatedIconHeart;
