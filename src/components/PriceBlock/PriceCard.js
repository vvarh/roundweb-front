import React from 'react';
import styled from 'styled-components';

const Smain = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
  position: relative;
  width: 100%;
  max-width: 900px;
  margin-top: 24px;
  padding: 12px;
  min-height: 90px;
  border: 2px solid #bb86fc;
  border-radius: 8px;
  box-sizing: border-box;
`;

const STitle = styled.span`
  font-size: 24px;
  font-weight: 500;
  margin: 8px;
`;

const SText = styled.span`
  margin: 8px;
  color: #ccc;
`;

const PriceCard = ({ title, text }) => (
  <Smain>
    <STitle>{title}</STitle>
    <SText>{text}</SText>
  </Smain>
);

export default PriceCard;
