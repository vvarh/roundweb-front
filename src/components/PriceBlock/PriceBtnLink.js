import React from 'react';
import styled, { keyframes } from 'styled-components';
import { Link } from 'react-router-dom';
import { ROUTER_LINK } from 'utils/const/routerLink';

export const backgroundAnim = keyframes`
  0% {
    background-color: transparent;
  }
  100% {
    background: rgba(255, 255, 255, 0.12);
  }
`;
export const colorAnim = keyframes`
  0% {
    color: #fff;
  }
  100% {
    color: #01e2a5;
  }
`;
export const positionAnimMobile = keyframes`
  0% {
    right: 4px;
  }
  50% {
    right: 16px;
  }
  100% {
    right: 4px;
  }
`;
export const positionAnimNotMobile = keyframes`
  0% {
    right: 50px;
  }
  50% {
    right: 36px;
  }
  100% {
    right: 50px;
  }
`;

const Smain = styled.div`
  cursor: pointer;
  position: relative;
  width: 100%;
  max-width: 900px;
  margin-top: 24px;
  padding: 12px 36px 12px 12px;
  min-height: 90px;
  border: 2px solid transparent;
  border-radius: 8px;
  box-sizing: border-box;

  :hover {
    border-color: #01e2a5;
    background-color: rgba(255, 255, 255, 0.12);
    animation: ${backgroundAnim} 0.5s ease-in-out;

    & .STitle {
      color: #01e2a5;
      animation: ${colorAnim} 0.5s ease-in-out;
    }

    & .SText {
      color: #fff;
    }

    & .SArrow line {
      stroke: #01e2a5;
    }
    & .SArrow {
      transform: translate(40px, -50%) scale(0.5);
    }
  }

  & .STitle {
    color: #fff;
    font-size: 24px;
    font-weight: 500;
  }

  & .SText {
    color: #888;
  }

  & .SArrow {
    position: absolute;
    top: 50%;
    right: 4px;
    transform: translate(40px, -50%) scale(0.5);
    transition: transform 0.5s ease-in-out;
    animation: ${positionAnimMobile} 3s ease infinite,
      ${colorAnim} 0.5s ease-in-out;
  }

  @media (min-width: 700px) {
    padding: 12px;
    & .SArrow {
      right: 50px;
      transform: translate(0, -50%) scale(0.5);
      animation: ${positionAnimNotMobile} 3s ease infinite,
        ${colorAnim} 0.5s ease-in-out;
    }

    :hover .SArrow {
      transform: translate(60px, -50%);
      transition: transform 0.5s ease-in-out;
    }
  }
`;

const PriceBtnLink = props => (
  <Smain>
    <Link to={ROUTER_LINK.quizPrice}>
      <p className="STitle">{props.title}</p>
      <p className="SText">{props.text}</p>
    </Link>

    <div className="SArrow">
      <svg
        width="80"
        height="100"
        viewBox="0 0 80 100"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <line x1="0" y1="0" x2="40" y2="50" stroke="#888888" strokeWidth="4" />
        <line
          x1="0"
          y1="100"
          x2="40"
          y2="50"
          stroke="#888888"
          strokeWidth="4"
        />
      </svg>
    </div>
  </Smain>
);

export default PriceBtnLink;
