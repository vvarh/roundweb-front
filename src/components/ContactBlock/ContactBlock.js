import React from 'react';
import styled from 'styled-components';

const STextBox = styled.div`
  max-width: 900px;
  margin: 0 auto;
  color: #fff;
  text-align: center;
`;

const SSloganH2 = styled.h2`
  color: #fff;
`;

const SEmail = styled.p`
  font-size: 24px;
  color: #01e2a5;
`;

const ContactBlock = () => {
  return (
    <STextBox>
      <SSloganH2>Напишите нам, чтобы узнать стоимость</SSloganH2>
      <SSloganH2>и сроки выполнения вашего проекта</SSloganH2>
      <SEmail>info@roundweb.ru</SEmail>
    </STextBox>
  );
};

export default ContactBlock;
