import React from 'react';
import styled, { keyframes } from 'styled-components';

const Smain = styled.div`
  position: relative;
  width: auto;
  text-align: center;
  z-index: 10;
`;

const transformAnim3 = keyframes`
  0% {transform: scaleY(1)}
  50% {transform: scaleY(0.5)}
  100% {transform: scaleY(1)}
`;

const SSVG = styled.svg`
  & path {
    animation: ${transformAnim3} 3s linear infinite;
  }
`;

const AnimatedBoxArc3 = () => (
  <Smain>
    <SSVG
      width="1920"
      height="150"
      viewBox="0 0 1920 150"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M0,0 C500,190, 1420,190, 1920,0"
        strokeWidth="3"
        stroke="#01e2a5"
      />
    </SSVG>
  </Smain>
);

export default AnimatedBoxArc3;
