import React from 'react';
import styled from 'styled-components';

const Smain = styled.div`
  position: relative;
  width: auto;
  overflow: hidden;
`;

const SSVGCircle = styled.svg`
  & circle:nth-child(1) {
    animation: opacityAnim 2s linear 3.5s forwards;
  }

  & circle:nth-child(2) {
    stroke-dasharray: 10;
    stroke-dashoffset: 200;

    animation: opacityBackAnim 2s linear 6s forwards,
      lineAnim 5s linear infinite;
  }
  & circle:nth-child(3) {
    stroke-dasharray: 1445;
    stroke-dashoffset: 1445;

    animation: lineAnimForCircle3 10s linear 10s forwards;
  }

  @keyframes opacityAnim {
    0% {
      opacity: 1;
    }
    100% {
      opacity: 0.2;
    }
  }
  @keyframes opacityBackAnim {
    0% {
      opacity: 0;
    }
    100% {
      opacity: 0.2;
    }
  }

  @keyframes lineAnim {
    100% {
      stroke-dashoffset: 0;
    }
  }

  @keyframes lineAnimForCircle3 {
    0% {
      stroke-dashoffset: 1445;
    }
    100% {
      stroke-dashoffset: 0;
    }
  }
`;

const AnimatedBoxLogo = () => (
  <Smain>
    <SSVGCircle
      width="500"
      height="500"
      viewBox="0 0 500 500"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <circle
        cx="250"
        cy="250"
        r="190"
        stroke="white"
        strokeWidth="5"
        opacity="0"
      />
      <circle
        cx="250"
        cy="250"
        r="210"
        stroke="white"
        strokeWidth="20"
        opacity="0"
      />
      <circle
        id="circle_dash"
        cx="250"
        cy="250"
        r="230"
        stroke="#01e2a5"
        strokeWidth="4"
      />
    </SSVGCircle>
  </Smain>
);

export default AnimatedBoxLogo;
