import React from 'react';
import styled from 'styled-components';

const Smain = styled.div`
  position: relative;
  width: auto;
  text-align: center;
  padding-top: 54px;
  z-index: 10;
`;
const SSVG = styled.svg`
  & path {
    stroke-dasharray: 10;
    stroke-dashoffset: 200;

    animation: line-anim 5s linear infinite;
  }

  @keyframes line-anim {
    to {
      stroke-dashoffset: 0;
    }
  }
`;

const AnimatedBox1 = () => (
  <Smain>
    <SSVG
      width="1920"
      height="60"
      viewBox="0 0 1920 60"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M0,0 C600,20 1320,20 1920,0"
        stroke="#01e2a5"
        strokeWidth="4px"
        opacity="0.5"
      />
    </SSVG>
    <SSVG
      width="1920"
      height="60"
      viewBox="0 0 1920 60"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M0,60 C600,-20 1320,-20 1920,60"
        stroke="#01e2a5"
        strokeWidth="4px"
        opacity="0.5"
      />
    </SSVG>
  </Smain>
);

export default AnimatedBox1;
