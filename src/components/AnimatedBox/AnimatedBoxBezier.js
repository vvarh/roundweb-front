import React from 'react';
import styled, { keyframes } from 'styled-components';

const Smain = styled.div`
  position: relative;
  width: auto;
  text-align: center;
  z-index: 10;
`;

const opacityAnim3 = keyframes`
  from {opacity: 0}
  to {opacity: 1}
`;

const SSVG = styled.svg`
  & path {
    opacity: 0;
    animation: ${opacityAnim3} 0s linear forwards;
  }
`;

class AnimatedBoxBezier extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      X_MIN: 0,
      X_MAX: 1920,
      Y1_MAX: 300,
      Y2_MIN: -150,
      TYPE2_Y_MIN: -150,
      TYPE2_Y_MAX: 300,
      X1: 0,
      Y1: 300,
      dir: +1, // +1 up    -1 down
      animationType: 1,
      timeInterval: 40,
      Cx1y1x2y2: `0,300, 1920,-150`
    };

    this.lastTimerID = null;
  }

  componentWillUnmount() {
    clearTimeout(this.lastTimerID);
  }

  componentDidMount() {
    this.lastTimerID = setTimeout(
      () => this.calcBezier(),
      this.state.timeInterval
    );
  }

  startTimer() {
    clearTimeout(this.lastTimerID);
    this.lastTimerID = setTimeout(
      () => this.calcBezier(),
      this.state.timeInterval
    );
  }

  calcBezier = () => {
    this.setState(prevState => {
      let { dir } = prevState;
      let { animationType } = prevState;
      const { timeInterval } = prevState;

      let X1 = 0;
      let Y1 = 300;

      switch (animationType) {
        case 1: {
          X1 = 750;
          Y1 = prevState.Y1 + dir * 10;

          if (Y1 > prevState.TYPE2_Y_MAX) {
            dir = -1;
            Y1 = prevState.TYPE2_Y_MAX;
          } else if (Y1 < prevState.TYPE2_Y_MIN) {
            dir = +1;
            Y1 = prevState.TYPE2_Y_MIN;
            animationType = 2;
          }
          break;
        }
        case 2: {
          X1 = prevState.X1 + dir * 50;

          if (X1 > prevState.X_MAX) {
            dir = -1;
            X1 = prevState.X_MAX;
          } else if (X1 < prevState.X_MIN) {
            dir = +1;
            X1 = prevState.X_MIN;
            animationType = 0;
          }
          break;
        }

        // animationType === 0
        default: {
          X1 = prevState.X1 + dir * 10;

          if (X1 > prevState.X_MAX) {
            dir = -1;
            X1 = prevState.X_MAX;
          } else if (X1 < prevState.X_MIN) {
            dir = +1;
            X1 = prevState.X_MIN;
            animationType = 1;
          }
          break;
        }
      }

      const X2 = prevState.X_MAX - X1;
      const Y2 = prevState.Y2_MIN + (prevState.Y1_MAX - Y1);
      const Cx1y1x2y2 = `${X1},${Y1}, ${X2},${Y2}`;

      return { X1, X2, Y1, Y2, dir, Cx1y1x2y2, animationType, timeInterval };
    });

    this.startTimer();
  };

  render() {
    const { Cx1y1x2y2 } = this.state;
    return (
      <Smain>
        <SSVG
          width="1920"
          height="150"
          viewBox="0 0 1920 150"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            fillRule="evenodd"
            clipRule="evenodd"
            d={`M0,75 C${Cx1y1x2y2}, 1920,75`}
            strokeWidth="3"
            stroke="#01e2a5"
          />
        </SSVG>
      </Smain>
    );
  }
}

export default AnimatedBoxBezier;
