import React from 'react';
import styled from 'styled-components';

const Smain = styled.div`
  position: relative;
  width: auto;
  text-align: center;
  z-index: 10;
`;

const SSVG = styled.svg`
  & path {
    opacity: 0;
  }
`;

const AnimatedBoxArc2 = () => (
  <Smain>
    <SSVG
      width="1920"
      height="150"
      viewBox="0 0 1920 150"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M0,75 C700,300, 1220,-150, 1920,75"
        strokeWidth="3"
        stroke="#01e2a5"
      />
    </SSVG>
  </Smain>
);

export default AnimatedBoxArc2;
