import React from 'react';
import styled, { keyframes } from 'styled-components';

const Smain = styled.div`
  position: relative;
  width: auto;
  text-align: center;
  z-index: 10;
`;

const opacityAnim3 = keyframes`
  8% {opacity: 0.5}
  10% {opacity: 1}
  15% {opacity: 0.8}
  20% {opacity: 1}
  25% {opacity: 0.5}
`;

const SSVG = styled.svg`
  & path {
    opacity: 0.5;
    animation: ${opacityAnim3} 10s linear infinite;
  }
`;

const AnimatedBoxArc = () => (
  <Smain>
    <SSVG
      width="1920"
      height="150"
      viewBox="0 0 1920 150"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M0,75 C1920,300, 0,-150, 1920,75"
        strokeWidth="3"
        stroke="#01e2a5"
      />
    </SSVG>
  </Smain>
);

export default AnimatedBoxArc;
