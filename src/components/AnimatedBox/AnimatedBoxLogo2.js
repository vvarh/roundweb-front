import React from 'react';
import styled from 'styled-components';

const Smain = styled.div`
  position: relative;
  width: auto;
  overflow: hidden;
`;

const SSVGCircle = styled.svg`
  & circle:nth-child(1) {
    animation: opacityAnim 2s linear 3.5s forwards;
  }

  & circle:nth-child(2) {
    stroke-dasharray: 10;
    stroke-dashoffset: 200;
    animation: opacityAnim 2s linear 3.5s forwards, lineAnim 10s linear infinite;
  }

  & #Group-3 circle:nth-child(1) {
    animation: opacityBackAnim 2s linear 8s forwards;
  }
  @keyframes opacityAnim {
    0% {
      opacity: 1;
    }
    100% {
      opacity: 0.2;
    }
  }
  @keyframes opacityBackAnim {
    0% {
      opacity: 0;
    }
    100% {
      opacity: 0.2;
    }
  }

  @keyframes lineAnim {
    100% {
      stroke-dashoffset: 0;
    }
  }

  @keyframes lineAnimForCircle3 {
    0% {
      stroke-dashoffset: 0;
    }
    100% {
      stroke-dashoffset: 1445;
    }
  }
`;

const AnimatedBoxLogo2 = () => (
  <Smain>
    <SSVGCircle
      width="500"
      height="500"
      viewBox="0 0 500 500"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <circle
        cx="250"
        cy="250"
        r="190"
        stroke="white"
        strokeWidth="5"
        opacity="0"
      />
      <circle
        cx="250"
        cy="250"
        r="210"
        stroke="white"
        strokeWidth="20"
        opacity="0"
      />

      <g id="Group-3" cx="250" cy="250">
        <circle
          id="circle_dash"
          cx="250"
          cy="250"
          r="230"
          stroke="white"
          opacity="0"
          strokeWidth="40"
          strokeDasharray="100 200"
        />
        <animateTransform
          attributeName="transform"
          type="rotate"
          from="360 250 250"
          to="0 250 250"
          begin="0s"
          dur="30s"
          repeatCount="indefinite"
        />
      </g>
    </SSVGCircle>
  </Smain>
);

export default AnimatedBoxLogo2;
