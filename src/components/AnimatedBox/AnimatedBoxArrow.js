import React from 'react';
import styled, { keyframes } from 'styled-components';

const Smain = styled.div`
  position: relative;
  width: auto;
  text-align: center;
  padding-top: 54px;
  z-index: 10;
`;

const opacityAnim2 = keyframes`
  from {opacity: 0}
  to {opacity: 1}
`;
export const positionAnimStart = keyframes`
  0% {
    bottom: 33px;
  }
  100% {
    bottom: 15px;
  }
`;
export const positionAnimMobile = keyframes`
  0% {
    bottom: 15px;
  }
  50% {
    bottom: 0px;
  }
  100% {
    bottom: 15px;
  }
`;

const SSVG = styled.svg`
  & path {
    opacity: 0;
    stroke-dasharray: 149;

    animation: ${opacityAnim2} 4s linear 5s forwards;
  }
`;
const SSVG2 = styled.svg`
  position: absolute;
  bottom: 33px;
  left: 0;
  right: 0;
  animation: ${positionAnimStart} 2s linear 9s forwards,
    ${positionAnimMobile} 3s linear 11s infinite;

  & path {
    opacity: 0;
    stroke-dasharray: 149;
    animation: ${opacityAnim2} 4s linear 5s forwards;
  }
`;

const AnimatedBoxArrow = () => (
  <Smain>
    <SSVG
      width="1920"
      height="60"
      viewBox="0 0 1920 60"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M0,0 L940,0 L960,20 L980,0 L1920,0"
        strokeWidth="3"
        stroke="#01e2a5"
      />
    </SSVG>
    <SSVG2
      width="1920"
      height="30"
      viewBox="0 0 1920 30"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M940,0 L960,20 L980,00"
        strokeWidth="3"
        stroke="#01e2a5"
      />
    </SSVG2>
  </Smain>
);

export default AnimatedBoxArrow;
