import React, { useState, useEffect } from 'react';
import styled from 'styled-components';

const WIDTH_CIRCLE = 250;

const SBoxClick = styled.div`
  position: absolute;
  width: ${props => `${props.width || 0}px`};
  height: ${props => `${props.width || 0}px`};
  opacity: ${props => `${props.width === WIDTH_CIRCLE ? 0 : 1}`};
  top: ${props => `${props.y || 0}px`};
  left: ${props => `${props.x || 0}px`};
  display: flex;
  justify-content: center;
  align-items: center;
  border: 4px solid #0c0921;
  border-radius: 50%;
  box-shadow: 0 -15px 15px rgba(255, 255, 255, 0.05),
    inset 0 -15px 15px rgba(255, 255, 255, 0.05), 0 15px 15px rgba(0, 0, 0, 0.5),
    inset 0 15px 15px rgba(0, 0, 0, 0.5);

  transform: translate(-50%, -50%);
  transition: width 1s linear, height 1s linear, opacity 1.2s ease-in;

  box-sizing: border-box;
`;

const ClickCircle = ({ x, y }) => {
  const [width, setWidth] = useState(0);

  useEffect(() => {
    const lastTID = setTimeout(() => {
      clearTimeout(lastTID);
      setWidth(WIDTH_CIRCLE);
    }, 60);

    return () => clearTimeout(lastTID);
  }, []);

  return <SBoxClick width={width} x={x} y={y} />;
};

export default ClickCircle;
