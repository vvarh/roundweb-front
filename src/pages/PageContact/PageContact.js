import React from 'react';
import styled from 'styled-components';
import ContactBlock from 'components/ContactBlock/ContactBlock';

const SPageContact = styled.div`
  width: auto;
`;

const SAnimBox = styled.div`
  width: 100%;
  height: 250px;
  overflow: hidden;
`;

const PageContact = () => {
  return (
    <SPageContact>
      <ContactBlock />

      <SAnimBox />
    </SPageContact>
  );
};

export default PageContact;
