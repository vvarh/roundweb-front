import React from 'react';
import styled from 'styled-components';
import PageHomePrice from '../PageHome/PageHomePrice';

const SHomePage = styled.div`
  width: auto;
  margin-bottom: 100px;
`;

const PagePrice = () => {
  return (
    <SHomePage>
      <PageHomePrice />
    </SHomePage>
  );
};

export default PagePrice;
