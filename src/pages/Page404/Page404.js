import React from 'react';
import { Link } from 'react-router-dom';
import { ROUTER_LINK } from 'utils/const/routerLink.js';
import styled from 'styled-components';

const Spage404 = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
`;

const Spage404Title = styled.p`
  margin: 0;
  font-size: 2em;
  text-align: center;
  color: #aaa;
`;

const Spage404H1 = styled.h1`
  font-size: 1.5em;
  text-align: center;
  color: #aaa;
`;

const Spage404Link = styled.p`
  cursor: pointer;
  font-size: 1.5em;
  text-align: center;
  text-decoration: none;
  color: #fff;

  :hover {
    color: #01e2a5;
    border-bottom: 1px solid #01e2a5;
  }
`;

class Page404 extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      text: {
        title404: `404`,
        h1: `Страница не найдена`,
        backLink: `Вернуться обратно`
      }
    };
  }

  render() {
    return (
      <Spage404>
        <Spage404Title>{this.state.text.title404}</Spage404Title>
        <Spage404H1>{this.state.text.h1}</Spage404H1>

        <Link to={ROUTER_LINK.home}>
          <Spage404Link>{this.state.text.backLink}</Spage404Link>
        </Link>
      </Spage404>
    );
  }
}

export default Page404;
