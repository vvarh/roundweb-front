import React from 'react';
import styled from 'styled-components';
import AnimatedBox1 from 'components/AnimatedBox/AnimatedBox1';
import AnimatedBoxArrow from 'components/AnimatedBox/AnimatedBoxArrow';
import AnimatedBoxBezier from 'components/AnimatedBox/AnimatedBoxBezier';
import Logo from 'components/Logo/Logo';
import AnimatedBoxLogo from 'components/AnimatedBox/AnimatedBoxLogo';
import AnimatedBoxLogo2 from 'components/AnimatedBox/AnimatedBoxLogo2';
import AnimatedBoxArc from 'components/AnimatedBox/AnimatedBoxArc';
import AnimatedBoxArc2 from 'components/AnimatedBox/AnimatedBoxArc2';
import AnimatedBoxArc3 from 'components/AnimatedBox/AnimatedBoxArc3';

const SHomePage = styled.div`
  width: auto;
`;

const SAnimBox = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 500px;
  overflow: hidden;
`;

const PageHome = () => {
  return (
    <SHomePage>
      <SAnimBox>
        <Logo />
      </SAnimBox>

      <SAnimBox>
        <AnimatedBoxLogo />
      </SAnimBox>

      <SAnimBox>
        <AnimatedBoxLogo2 />
      </SAnimBox>

      <SAnimBox>
        <AnimatedBoxArrow />
      </SAnimBox>

      <SAnimBox>
        <AnimatedBoxBezier />
      </SAnimBox>

      <SAnimBox>
        <AnimatedBox1 />
      </SAnimBox>

      <SAnimBox>
        <AnimatedBoxArc />
      </SAnimBox>

      <SAnimBox>
        <AnimatedBoxArc2 />
      </SAnimBox>

      <SAnimBox>
        <AnimatedBoxArc3 />
      </SAnimBox>
    </SHomePage>
  );
};

export default PageHome;
