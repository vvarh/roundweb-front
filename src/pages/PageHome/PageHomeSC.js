import styled from 'styled-components';

export const SHomePage = styled.div`
  position: relative;
  width: auto;
  overflow: hidden;
`;

export const STopBlock = styled.div`
  width: auto;
  min-height: 100vh;
`;

export const SAnimBox = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 250px;
  overflow: hidden;
`;

export const STextBox = styled.div`
  max-width: 900px;
  margin: 0 auto;
  color: #fff;
  text-align: center;
`;

export const SSloganH1 = styled.h1`
  font-size: 1.4em;
  color: #fff;

  @media (min-width: 500px) {
    font-size: 2em;
  }
`;

export const SSloganH2 = styled.h2`
  color: #fff;
`;

export const STitleP = styled.p``;
