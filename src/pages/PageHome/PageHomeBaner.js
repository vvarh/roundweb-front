import React from 'react';
import styled from 'styled-components';
import { useSelector } from 'react-redux';
import Logo from 'components/Logo/Logo';
// import AnimatedBoxLogo from 'components/AnimatedBox/AnimatedBoxLogo';
import AnimatedBoxLogo2 from 'components/AnimatedBox/AnimatedBoxLogo2';

const Smain = styled.div`
  position: relative;
  display: flex;
  width: 100%;
  height: 150px;
  padding: 0;
  margin-bottom: 24px;
  z-index: 10;

  @media (min-width: 500px) {
    height: 500px;
  }
`;

const SLogoBox = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
`;
const SAnimatedBox = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
`;

/* time animation
3s        - logo
3.5-5.5s  - 1 circle
6-8s      - 2 circle
10-20s    - 3 circle load
*/

const PageHomeBaner = () => {
  const isNotMobile = useSelector(state => state.appSettings.isNotMobile);
  return (
    <Smain>
      <SLogoBox>
        <Logo />
      </SLogoBox>

      {/* render animation for tabled or decktop */}
      {isNotMobile && (
        <SAnimatedBox>
          {/* <AnimatedBoxLogo /> */}
          <AnimatedBoxLogo2 />
        </SAnimatedBox>
      )}
    </Smain>
  );
};

export default PageHomeBaner;
