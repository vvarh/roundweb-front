import React, { useState, useEffect } from 'react';
import ClickCircle from 'components/ClickCircle/ClickCircle';

const TOUCH_FIX_Y = 75;
const CLICK_FIX_Y = 50;

const PageHomeClicker = () => {
  const [clickArr, setClickArr] = useState([]);

  useEffect(() => {
    let lastClearTimeoutID;
    let firstClick = true;

    const clearClickArr = () => {
      setClickArr([]);
    };

    const onBodyClick = e => {
      const touch = e.changedTouches && e.changedTouches[0];
      const x = touch ? touch.pageX : e.pageX;
      const y = touch ? touch.pageY - TOUCH_FIX_Y : e.pageY - CLICK_FIX_Y;

      setClickArr(prevState => {
        const id = String(prevState.length);

        clearTimeout(lastClearTimeoutID);
        lastClearTimeoutID = setTimeout(() => {
          clearTimeout(lastClearTimeoutID);
          clearClickArr();
        }, 1500);

        return [...prevState, { id, x, y, isActive: true }];
      });
    };

    const onBodyTouchStart = e => {
      if (firstClick) {
        firstClick = false;
        document.removeEventListener('click', onBodyClick);
      }

      onBodyClick(e);
    };

    document.addEventListener('click', onBodyClick);
    document.addEventListener('touchstart', onBodyTouchStart);

    return () => {
      document.removeEventListener('click', onBodyClick);
      document.removeEventListener('touchstart', onBodyTouchStart);

      clearTimeout(lastClearTimeoutID);
    };
  }, []);

  return (
    <div>
      {clickArr.map(it => (
        <ClickCircle key={it.id} id={it.id} x={it.x} y={it.y} />
      ))}
    </div>
  );
};

export default PageHomeClicker;
