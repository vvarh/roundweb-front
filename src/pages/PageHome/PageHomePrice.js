import React from 'react';
import styled from 'styled-components';
// import PriceBtnLink from 'components/PriceBlock/PriceBtnLink';
import PriceCard from 'components/PriceBlock/PriceCard';

const SMain = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 100%;
`;

const STextBox = styled.div`
  max-width: 900px;
  margin: 0 auto;
  color: #fff;
  text-align: center;
`;

const SSloganH2 = styled.h2`
  color: #fff;
`;

const goodPrices = [
  {
    title: `Лендинг`,
    text: `Создание высокоэффективного лендинга.`
  },
  {
    title: `сайт-КВИЗ`,
    text: `Вовлечение посетителей. Интерактивное представление информации. Удобный интерфейс.`
  },
  {
    title: `Интернет магазин`,
    text: `Корзина пользователя. Кабинет менеджера для управления товаром.`
  },
  {
    title: `Автоматизация производства`,
    text: `Сбор данных, представление данных, анализ и управление.`
  },
  {
    title: `Мобильные приложения`,
    text: `ios, android`
  }
];

const PageHomePrice = () => {
  return (
    <STextBox>
      <SSloganH2>Услуги</SSloganH2>

      <SMain>
        {goodPrices.map(it => (
          <PriceCard key={it.title} title={it.title} text={it.text} />
        ))}
      </SMain>

      {/* <PriceBtnLink
        title="Узнайте стоимость в два клика!"
        text="Пожалуйста, пройдите короткий опрос, чтобы получить лучшее предложение"
      /> */}
    </STextBox>
  );
};

export default PageHomePrice;
