import React from 'react';
import styled, { keyframes } from 'styled-components';
import marketing from 'assets/icons/marketing.svg';
import rocket from 'assets/icons/rocket.svg';
import siteCreate from 'assets/icons/siteCreate.svg';
import siteSearch from 'assets/icons/siteSearch.svg';

const SMain = styled.div`
  margin: 100px auto 0;
  color: #fff;
  text-align: center;
`;

const SSloganH2 = styled.h2`
  margin-bottom: 54px;
  color: #fff;
`;

const SCardBox = styled.ul`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: flex-start;
  flex-wrap: wrap;
  width: 100%;
`;

const SCardMain = styled.li`
  list-style-type: none;
  max-width: 250px;
`;

const transformAnim1 = keyframes`
  10% {transform: scale(1)}
  12% {transform: scale(1.2)}
  17% {transform: scale(0.9)}
  20% {transform: scale(1)}
`;
const transformAnim2 = keyframes`
  30% {transform: scale(1)}
  32% {transform: scale(1.2)}
  37% {transform: scale(0.9)}
  40% {transform: scale(1)}
`;
const transformAnim3 = keyframes`
  50% {transform: scale(1)}
  52% {transform: scale(1.2)}
  57% {transform: scale(0.9)}
  60% {transform: scale(1)}
`;
const transformAnim4 = keyframes`
  70% {transform: scale(1)}
  72% {transform: scale(1.2)}
  77% {transform: scale(0.9)}
  80% {transform: scale(1)}
`;

const transformAnim = [
  transformAnim1,
  transformAnim2,
  transformAnim3,
  transformAnim4
];

const SCardImg = styled.img`
  width: 75px;
  height: 75px;
  animation: ${props => transformAnim[props.i]} 8s linear infinite;
`;
// 10s
// 1-1.5
// 2-2.5
// 3-3.5
// 4-4.5

const SCardTitle = styled.p`
  color: #fff;
  font-size: 16px;
`;
const SCardText = styled.p`
  color: #888;
  font-size: 14px;
`;

const goodPrices = [
  {
    icon: `siteCreate`,
    title: `Создание сайтов любой сложности`,
    text: `От лендинга до функционального интернет-магазина с интерактивом`,
    altImg: `Разработка`
  },
  {
    icon: `rocket`,
    title: `SEO-продвижение`,
    text: `Комлпксный подход и полная прозрачность`,
    altImg: `В топ`
  },
  {
    icon: `marketing`,
    title: `Интернет-маркетинг`,
    text: `Настройка рекламных команий`,
    altImg: `Таргетинг`
  },
  {
    icon: `siteSearch`,
    title: `Аудит и поддержка сайтов`,
    text: `Проведем аудит, поможем оптимизировать работу сайта. Техническая поддержка на весь период работ`,
    altImg: `Оптимизация`
  }
];

const iconList = {
  marketing,
  rocket,
  siteCreate,
  siteSearch
};

const Card = ({ i, icon, altImg, title, text }) => (
  <SCardMain>
    <SCardImg i={i} src={iconList[icon]} alt={altImg} />

    <SCardTitle>{title}</SCardTitle>

    <SCardText>{text}</SCardText>
  </SCardMain>
);

const PageHomeMainDir = () => {
  return (
    <SMain>
      <SSloganH2>ОСНОВНЫЕ НАПРАВЛЕНИЯ</SSloganH2>

      <SCardBox>
        {goodPrices.map((it, i) => (
          // eslint-disable-next-line react/jsx-props-no-spreading
          <Card key={it.title} i={i} {...it} />
        ))}
      </SCardBox>
    </SMain>
  );
};

export default PageHomeMainDir;
