import React from 'react';
import AnimatedBox1 from 'components/AnimatedBox/AnimatedBox1';
import AnimatedBoxArrow from 'components/AnimatedBox/AnimatedBoxArrow';
import AnimatedBoxArc from 'components/AnimatedBox/AnimatedBoxArc';
import ContactBlock from 'components/ContactBlock/ContactBlock';
import PageHomeBaner from './PageHomeBaner';
import PageHomePrice from './PageHomePrice';
import PageHomeMainDir from './PageHomeMainDir';
import PageHomeOurAdvantages from './PageHomeOurAdvantages';
import PageHomeClicker from './PageHomeClicker';
import {
  SHomePage,
  STopBlock,
  SAnimBox,
  STextBox,
  SSloganH1,
  SSloganH2,
  STitleP
} from './PageHomeSC';

const PageHome = () => {
  return (
    <SHomePage>
      <STopBlock>
        <PageHomeBaner />

        <STextBox>
          <SSloganH1>РАЗРАБОТКА. ПОДДЕРЖКА. ПРОДВИЖЕНИЕ</SSloganH1>
        </STextBox>

        <SAnimBox>
          <AnimatedBoxArrow />
        </SAnimBox>
      </STopBlock>

      <STextBox>
        <SSloganH2>Разработка программного обеспечения на заказ</SSloganH2>
        <STitleP>
          В сотрудничестве с вами мы создаем программный продукт, функциональные
          возможности которого полностью отвечают уникальным потребностям вашего
          бизнеса и особенностям услуг.
        </STitleP>
        <STitleP>Разработка качественного программного обеспечения</STitleP>
      </STextBox>

      <SAnimBox>
        <AnimatedBoxArc />
      </SAnimBox>

      <PageHomeMainDir />

      <SAnimBox />

      <PageHomePrice />

      <SAnimBox>
        <AnimatedBox1 />
      </SAnimBox>

      <PageHomeOurAdvantages />

      <SAnimBox />

      <ContactBlock />

      <SAnimBox />

      <PageHomeClicker />
    </SHomePage>
  );
};

export default PageHome;
