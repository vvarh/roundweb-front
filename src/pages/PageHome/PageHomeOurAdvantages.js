import React from 'react';
import styled from 'styled-components';
import AnimatedIconCheckbox from 'components/AnimatedIcon/AnimatedIconCheckbox';
import AnimatedIconLamp from 'components/AnimatedIcon/AnimatedIconLamp';
import AnimatedIconWallet from 'components/AnimatedIcon/AnimatedIconWallet';
import AnimatedIconHeart from 'components/AnimatedIcon/AnimatedIconHeart';
import AnimatedIconMedal from 'components/AnimatedIcon/AnimatedIconMedal';
import AnimatedIconClock from 'components/AnimatedIcon/AnimatedIconClock';

const SMain = styled.div`
  margin: 100px auto 0;
  color: #fff;
  text-align: center;
`;

const SSloganH2 = styled.h2`
  margin-bottom: 54px;
  color: #fff;
`;

const SCardBox = styled.ul`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: flex-start;
  flex-wrap: wrap;
  width: 100%;
`;

const SCardMain = styled.li`
  list-style-type: none;
  max-width: 200px;
`;

const SCardImg = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 75px;
`;

const SCardText = styled.p`
  padding: 8px;
  color: #fff;
  font-size: 14px;
`;

const goodPrices = [
  {
    icon: <AnimatedIconHeart />,
    text: `Внимательное отношение к клиентам`
  },
  {
    icon: <AnimatedIconMedal />,
    text: `Оплата продвижения за результат`
  },
  {
    icon: <AnimatedIconLamp />,
    text: `Индивидуальный дизайн`
  },
  {
    icon: <AnimatedIconWallet />,
    text: `Лучшее предложение за эти деньги`
  },
  {
    icon: <AnimatedIconCheckbox />,
    text: `Соблюдение веб-стандартов`
  },
  {
    icon: <AnimatedIconClock />,
    text: `Разумные сроки разработки`
  }
];

const Card = ({ item }) => (
  <SCardMain>
    <SCardImg>{item.icon}</SCardImg>

    <SCardText>{item.text}</SCardText>
  </SCardMain>
);

const PageHomeOurAdvantages = () => {
  return (
    <SMain>
      <SSloganH2>НАШИ ПРЕИМУЩЕСТВА</SSloganH2>

      <SCardBox>
        {goodPrices.map(it => (
          <Card key={it.text} item={it} />
        ))}
      </SCardBox>
    </SMain>
  );
};

export default PageHomeOurAdvantages;
