import { assert } from 'chai';
import { isValide } from './isValide.js';

describe('isValide', () => {
  describe('isValide email', () => {
    it('valid email', () => {
      assert.equal(isValide.email('test@mail.ru'), true);
      assert.equal(isValide.email('test123@mail.ru'), true);
      assert.equal(isValide.email('test+1@mail.ru'), true);
    });

    it('not valid email', () => {
      assert.equal(isValide.email('test'), false);
      assert.equal(isValide.email('test@'), false);
      assert.equal(isValide.email('test@mail'), false);
      assert.equal(isValide.email('test@mail.r'), false);
    });
  });

  describe('isValide password', () => {
    it('valid password length > 6', () => {
      assert.equal(isValide.password('123456'), true);
    });
    it('valid password', () => {
      assert.equal(isValide.password('qwe123'), true);
    });

    it('not valid password length < 6', () => {
      assert.equal(isValide.password('12345'), false);
    });
  });

  describe('isValide phoneNumber', () => {
    it('valid phoneNumber', () => {
      assert.equal(isValide.phoneNumber('123456'), true);
    });
  });

  describe('isValide textNumber', () => {
    it('valid textNumber ', () => {
      assert.equal(isValide.textNumber('123456'), true);
    });

    it('not valid textNumber ', () => {
      assert.equal(isValide.textNumber('qwe123'), false);
    });
  });

  describe('isValide textNumberMoreLength', () => {
    it('valid textNumberMoreLength ', () => {
      assert.equal(isValide.textNumberMoreLength('123456', 3), true);
    });
  });
});
