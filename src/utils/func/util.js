/**
 * «Тормозилка», которая возвращает обёртку,
 * передающую вызов cb не чаще, чем раз в ms миллисекунд.
 *
 * @param cb
 * @param ms
 * @return {wrapper}
 */
export const throttle = (cb, ms) => {
  let isThrottled = false;
  let savedArgs = null;
  let savedThis = null;

  function wrapper(...arg) {
    if (isThrottled) {
      savedArgs = arg;
      savedThis = this;
      return;
    }

    cb.apply(this, arg);

    isThrottled = true;

    setTimeout(() => {
      isThrottled = false;
      if (savedArgs) {
        wrapper.apply(savedThis, savedArgs);
        savedArgs = null;
        savedThis = null;
      }
    }, ms);
  }

  return wrapper;
};

// Возвращает функцию, которая не будет срабатывать, пока продолжает вызываться.
// Она сработает только один раз через N миллисекунд после последнего вызова.
// Если ей передан аргумент `immediate`, то она будет вызвана один раз сразу после
// первого запуска.
export const debounce = (func, wait, immediate) => {
  let timeout;

  function wrapper(...args) {
    const context = this;

    const later = () => {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };

    const callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) func.apply(context, args);
  }

  return wrapper;
};
