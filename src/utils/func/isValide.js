/* eslint-disable no-restricted-globals */
import { EMAIL_REGEX, PASSWORD_REGEX, PHONE_REGEX } from 'utils/const/regex.js';

// after check return bool
export const isValide = {
  email: email => EMAIL_REGEX.test(email),
  password: password => PASSWORD_REGEX.test(password),
  passwordsMatch: (password1, password2) => password1 !== password2,
  phoneNumber: phoneNumber => PHONE_REGEX.test(phoneNumber),
  textNumber: text => !isNaN(text),
  textNumberMoreLength: (text, length) => isNaN(text) || text.length > length
};
