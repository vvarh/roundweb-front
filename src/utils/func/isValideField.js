import { isValide } from './isValide.js';

// after check return errorsText
export const isValideField = {
  email: value => (isValide.email(value) ? undefined : 'Invalid email address'),
  required: value => (value ? undefined : 'Required'),
  password: value =>
    isValide.password(value) ? undefined : 'Invalid password',
  passwordsMatch: (value, allValues) =>
    isValide.passwordsMatch(value, allValues.newPassword)
      ? "Passwords don't match"
      : undefined
};
