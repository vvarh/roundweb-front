export const EMAIL_REGEX = /^\w+([+.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,8})+$/;
export const PASSWORD_REGEX = /\w{6,}/;
export const PHONE_REGEX = /^\s*(?:\+?(\d{1,3}))?([-. (]*(\d{3})[-. )]*)?((\d{3})[-. ]*(\d{2,4})(?:[-.x ]*(\d+))?)\s*$/;
export const LINE_BREAK = /\r\n|\r|\n/g;
