// const baseUrl = process.env.NODE_ENV === `development` ? `/` : `/whitu/`;
const baseUrl = `/`;

export const ROUTER_LINK = {
  home: `${baseUrl}`,
  services: `${baseUrl}services`,
  contacts: `${baseUrl}contacts`,
  quizPrice: `${baseUrl}quiz-price`,
  policy: `${baseUrl}privacy-policy`,
  oferta: `${baseUrl}oferta`
};
