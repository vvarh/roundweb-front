import PageHome from 'pages/PageHome/PageHome';
import PageContact from 'pages/PageContact/PageContact';
import PagePrice from 'pages/PagePrice/PagePrice';
import PagePolicy from 'pages/PagePolicy/PagePolicy';
import PageOferta from 'pages/PageOferta/PageOferta';
import Page404 from 'pages/Page404/Page404';
import PageAnim from 'pages/PageAnim/PageAnim';

import { ROUTER_LINK } from 'utils/const/routerLink.js';

const routersCommon = [
  {
    path: ROUTER_LINK.home,
    component: PageHome,
    exact: true
  },
  {
    path: `/a`,
    component: PageAnim,
    exact: true
  },
  {
    path: ROUTER_LINK.contacts,
    component: PageContact,
    exact: true
  },
  {
    path: ROUTER_LINK.services,
    component: PagePrice,
    exact: true
  },
  {
    path: ROUTER_LINK.policy,
    component: PagePolicy,
    exact: true
  },
  {
    path: ROUTER_LINK.oferta,
    component: PageOferta,
    exact: true
  },
  {
    component: Page404
  }
];

export default routersCommon;
