import React from 'react';
import PropTypes from 'prop-types';
import { Switch, Route, Router, Redirect } from 'react-router-dom';
import LayoutMain from 'layout/LayoutMain';
import { ROUTER_LINK } from 'utils/const/routerLink.js';
import { history } from './history';
import routersCommon from './routersCommon.js';
import routersPrivate from './routersPrivate.js';

const RouterH = props => (
  <Router history={history}>
    <LayoutMain>
      <Switch>
        {/* protection router links */}
        {/* if not have token => Login Page */}
        {routersPrivate.map(
          ({ component: ComponentRoute, path, sensitive, exact }) => (
            <Route
              key={`${path}`}
              path={path}
              exact={exact}
              sensitive={sensitive}
              render={routeProps =>
                props.token ? (
                  // eslint-disable-next-line react/jsx-props-no-spreading
                  <ComponentRoute {...routeProps} />
                ) : (
                  <Redirect to={ROUTER_LINK.login} />
                )
              }
            />
          )
        )}

        {/* protection router links */}
        {/* if not have token => not have route => 404 Page */}
        {/* {props.token &&
        routersPrivate.map(it => (
          <Route
            key={`${it.path}`}
            path={it.path}
            component={it.component}
            sensitive={it.sensitive}
            exact={it.exact}
          />
        ))} */}

        {/* common router links */}
        {routersCommon.map(it => (
          <Route
            key={`${it.path}`}
            path={it.path}
            component={it.component}
            sensitive={it.sensitive}
            exact={it.exact}
          />
        ))}
      </Switch>
    </LayoutMain>
  </Router>
);

RouterH.propTypes = {
  token: PropTypes.string
};

export default RouterH;
