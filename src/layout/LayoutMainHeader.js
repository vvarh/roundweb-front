import React from 'react';
import { Link } from 'react-router-dom';
import { ROUTER_LINK } from 'utils/const/routerLink.js';
import { SHeader, SLogo, SMenu, SMenuItemLink } from './LayoutMainHeaderSC';

const LayoutMainHeader = () => {
  return (
    <SHeader>
      <Link to={ROUTER_LINK.home}>
        <SLogo>RoundWeb</SLogo>
      </Link>

      <SMenu>
        <Link to={ROUTER_LINK.services}>
          <SMenuItemLink>Услуги</SMenuItemLink>
        </Link>
        <Link to={ROUTER_LINK.contacts}>
          <SMenuItemLink>Контакты</SMenuItemLink>
        </Link>
      </SMenu>
    </SHeader>
  );
};

export default LayoutMainHeader;
