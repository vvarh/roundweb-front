import React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { ROUTER_LINK } from 'utils/const/routerLink.js';

const SFooter = styled.footer`
  width: 100%;
  padding: 12px 12px 36px 12px;
  border-top: 2px dashed #bb86fc;

  box-sizing: border-box;
`;

const SLogo = styled.p`
  color: #fff;
`;

const SMenu = styled.div`
  display: flex;
  flex-direction: column;
`;

const SMenuItemLink = styled.span`
  cursor: pointer;
  display: flex;
  width: max-content;
  margin: 8px;
  color: #fff;
  border-bottom: 1px solid transparent;

  :hover {
    color: #01e2a5;
    border-bottom-color: #01e2a5;
  }
`;

const SMenuBottom = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  width: 100%;
  margin-top: 18px;
  box-sizing: border-box;
`;

const SMenuBottomLink = styled.span`
  cursor: pointer;
  display: flex;
  width: max-content;
  margin: 8px;
  font-size: 12px;
  color: #fff;
  border-bottom: 1px solid transparent;

  :hover {
    color: #01e2a5;
    border-bottom-color: #01e2a5;
  }
`;

const LayoutMainFooter = () => {
  return (
    <SFooter>
      <Link to={ROUTER_LINK.home}>
        <SLogo>RoundWeb</SLogo>
      </Link>

      <SMenu>
        <Link to={ROUTER_LINK.services}>
          <SMenuItemLink>Услуги</SMenuItemLink>
        </Link>
        <Link to={ROUTER_LINK.contacts}>
          <SMenuItemLink>Контакты</SMenuItemLink>
        </Link>
      </SMenu>

      <SMenuBottom>
        <Link to={ROUTER_LINK.oferta}>
          <SMenuBottomLink>Публичная оферта</SMenuBottomLink>
        </Link>
        <Link to={ROUTER_LINK.policy}>
          <SMenuBottomLink>Политика конфиденциальности</SMenuBottomLink>
        </Link>
      </SMenuBottom>
    </SFooter>
  );
};

export default LayoutMainFooter;
