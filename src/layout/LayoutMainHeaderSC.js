import styled from 'styled-components';

export const SHeader = styled.header`
  width: auto;

  @media (min-width: 400px) {
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding: 0 36px;
  }
`;

export const SLogo = styled.p`
  color: #fff;
`;

export const SMenu = styled.div`
  display: flex;
  justify-content: space-around;

  @media (min-width: 400px) {
    justify-content: space-between;
  }
`;

export const SMenuItemLink = styled.p`
  cursor: pointer;
  margin: 0 8px;
  color: #fff;

  :hover {
    color: #01e2a5;
    border-bottom: 1px solid #01e2a5;
  }
`;
