import React from 'react';
import styled from 'styled-components';
import LayoutMainHeader from './LayoutMainHeader';
import LayoutMainFooter from './LayoutMainFooter';

const SMain = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  width: auto;
  min-height: 100vh;
  padding: 4px;
  box-sizing: border-box;
  background-color: #131535;
`;

const SMainTop = styled.main`
  display: flex;
  flex-direction: column;
  width: auto;
`;

function LayoutMain(props) {
  return (
    <SMain>
      <LayoutMainHeader />

      <SMainTop>{props.children}</SMainTop>

      <LayoutMainFooter />
    </SMain>
  );
}

export default LayoutMain;
