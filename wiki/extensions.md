#vs code extensions

- Auto Close Tag
- Auto Rename Tag
- Bracket Pair Colorized
- Color Highlight
- CSS Peek
- Debugger for Chrome
- EditorConfig for VS Code
- ES7 React/Redux/GraphQL/React-Native snipet
- ESLint
- Git Graph
- Highlight Matching Tag
- Indent-rainbow
- InteliliSense for CSS class names in HTML
- JS Parametr Annotations
- Path intellisense
- Prettier - Code formatter
- React Native Tools
- SVG Viewer
- Turbo Console Log
- Vetur
- vscode-styled-components
