module.exports = {
  parser: 'babel-eslint',
  plugins: ['react', 'react-hooks', 'prettier'],
  extends: ['airbnb', "plugin:prettier/recommended", 'prettier/react'],
  env: {
    es6: true,
    jest: true,
    browser: true,
    node: true
  },
  globals: {
    __DEV__: true,
    fetch: true
  },
  rules: {
    "prettier/prettier": ["error", {"singleQuote": true, "parser": "flow"}],
    'no-plusplus': 0,
    'no-use-before-define': 0,
    'react/jsx-filename-extension': 0,
    'react/prop-types': 0,
    'comma-dangle': 0,
    'import/prefer-default-export': 0,
    'import/no-unresolved': 0,
    'import/no-extraneous-dependencies': 0,
    'import/extensions': 0,
    'react/destructuring-assignment': [
      0,
      'always',
      {
        ignoreClassFields: 0
      }
    ],
    'react/require-default-props': 0,
    'react/forbid-prop-types': 0,
    'jsx-a11y/anchor-is-valid': ['warn', { aspects: ['invalidHref'] }],
    'react/no-underscore-dangle': 0,
    'no-underscore-dangle': 0,
    'jsx-a11y/label-has-for': 0,
    'no-nested-ternary': 0,
    'react/sort-comp': 0,

    'react-hooks/rules-of-hooks': 'error',
    'react-hooks/exhaustive-deps': 'warn'
  }
};
